# windows-helpers

usefull things if you are doomed.

## description

scripts and things for windows (10)

## readme.notes

### additional applications
* https://conemu.github.io/ - ConEmu-Maximus5 handy, comprehensive, fast and reliable terminal window where you may host any console application developed either for WinAPI (cmd, powershell, far) or Unix PTY (cygwin, msys, wsl bash).

### environment variables in cmd.exe
notation is %VARIABLE%

display all variables: set

you should use echo without quotes: echo %HOMEDRIVE%%HOMEPATH%

see also: https://ss64.com/nt/syntax-variables.html
